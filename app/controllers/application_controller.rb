class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :validate_role

  def validate_role
    if user_login_signed_in?
      case current_user_login.role.downcase
        when "hos"
          allowed_controllers = ['devise/sessions', 'hos', 'hos/products']
          if !allowed_controllers.include?(params[:controller])
            redirect_to :controller => "/hos", :action => "index"
          end
        when "hr"
          allowed_controllers = ['devise/sessions', 'hr']
          if !allowed_controllers.include?(params[:controller])
            redirect_to :controller => "/hr", :action => "index"
          end
      end
    end
  end

  def after_sign_in_path_for(resource)
    case resource.role.downcase
      when resource.role == 'admin'
        admin_path
      when resource.role == 'hos'
        hos_path
      when resource.role == 'hr'
        hr_path
      else
        admin_path
    end
  end

end
