class Hos::ProductsController < ApplicationController

  layout "hos"

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:notice] = "Product successfully created!"
      status = 1
    else
      status = 0
    end
    render json: { status: status, product: @product }
  end

  def edit
    @product = Product.find(params[:id])
  end

  private

  def product_params
    params.require(:product).permit(:name, :premium, :coverage_type, :trip_type, :insurer_name)
  end

end
