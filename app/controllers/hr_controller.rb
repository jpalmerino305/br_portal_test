class HrController < ApplicationController

  layout "hr"

  before_filter :authenticate_user_login!

  def index

    @products = Product.select(:insurer_name).distinct

  end

  def filter
    @products = Product.where(["insurer_name = ? AND coverage_type = ? AND trip_type = ?", params[:insurer_name], params[:coverage_type], params[:trip_type]])
    render json: { products: @products }
  end

end