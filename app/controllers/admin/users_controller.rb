class Admin::UsersController < ApplicationController


  layout "admin"

  before_filter :authenticate_user_login!

  def index
    check_role_param(params['role'])
    case params['role']
      when "head-of-sales"
        @users = HosProfile.all
        @title = "Head of Sales Users List"
      when "hr"
        @users = HrProfile.all
        @title = "HR Users List"
    end
  end

  def new
    check_role_param(params['role'])
    @user = HosProfile.new

    case params['role']
      when "head-of-sales"
        @role = "hos"
        @title = "New Head of Sales User"
      when "hr"
        @role = "hr"
        @title = "New HR user"
    end
  end

  def create
    check_role_param(params['role'])
    role = params[:user][:role]

    if role == "hos"
      @user = HosProfile.new(user_params)
      success_notice = "Successfully created Head of Sales user!"
    elsif role == "hr"
      @user = HrProfile.new(user_params)
      success_notice = "Successfully created HR user!"
    end

    if @user.save

      login = UserLogin.new
      login.email = user_params[:email]
      login.password = params[:password]
      login.role = role
      login.save

      login.loginable = @user
      login.save

      flash[:notice] = success_notice

      status = 1
    else

      status = 0
    end

    render json: { status: status, user: @user }
  end

  def edit
    check_role_param(params['role'])
    case params['role']
      when "head-of-sales"
        @user = HosProfile.find(params[:id])
        @role = "hos"
        @title = "Edit Head of Sales User"
      when "hr"
        @user = HrProfile.find(params[:id])
        @role = "hr"
        @title = "Edit HR user"
    end
  end

  def update
    @user = HosProfile.find(params[:id])
  end

  def validate_email_uniqueness
    errors = []
    user_login = UserLogin.where(["email = ?", params[:email]])
    if user_login.count > 0
      errors.push('Email already exist')
    end
    render json: { errors: errors }
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :date_of_birth, :email, :nric, :gender, :role)
  end

  def check_role_param(role)
    valid_role_param = ["head-of-sales", "hr"]
    if !valid_role_param.include?(role)
      redirect_to controller: "/admin", action: "index"
    end
  end


end
