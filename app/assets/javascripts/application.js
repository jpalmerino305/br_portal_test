// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui/widgets/datepicker
//= require jquery-number/jquery.number.min
//= require_tree .

$(document).ready(function(){

  var min_age = 20;
  var max_age = 90;

  $('.datepicker').datepicker({
    dateFormat: "dd/mm/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: '-' + (max_age + 1) + ':-' + (min_age + 1),
    defaultDate: '-' + (max_age + 1) + 'y'
    // buttonImage: 'contact/calendar/calendar.gif',
    // buttonImageOnly: true,
  });


  $('#user-form').on('submit', function(e){
    e.preventDefault();
    $('#form-errors').html('').hide();

    // $('#loader').show();
    setTimeout(function(){}, 1000);

    var form = $(this);
    var authenticity_token = $('input[name="authenticity_token"]');
    var role = $('input[name="user[role]"]');
    var first_name = $('input[name="user[first_name]"]');
    var last_name = $('input[name="user[last_name]"]');
    var date_of_birth = $('input[name="user[date_of_birth]"]');
    var email = $('input[name="user[email]"]');
    var nric = $('input[name="user[nric]"]');
    var gender = $('input[name="user[gender]"]');
    var password = $('input[name="password"]');

    var errors = validateForm();
    var html_errors = '';

    if (errors.length > 0){
      for (var i = 0; i < errors.length; i ++){
        html_errors += '<li>' + errors[i] + '</li>';
      }
      $('#form-errors').html(html_errors).show();
    } else {
      console.log('Valid form....');

      var dob_arr = date_of_birth.val().split('/');

      var form_data = {
        authenticity_token: authenticity_token.val(),
        user:{
          role: role.val(),
          first_name: first_name.val(),
          last_name: last_name.val(),
          date_of_birth: dob_arr[2] + '-' + dob_arr[1] + '-' + dob_arr[0],
          email: email.val(),
          nric: nric.val(),
          gender: $('input[name="user[gender]"]:checked').val()
        },
        password: password.val()
      }

      $.ajax({
        type: 'POST',
        async: false,
        url: form.attr('action'),
        data: form_data,
        success: function(result){
          if (result.status == 1){
            window.location.href = form.attr('redirect-url');
          }
        },
        error: function(result){
        }
      });

    }

    return false;
  });

  function validateForm(){

    var first_name = $('input[name="user[first_name]"]');
    var last_name = $('input[name="user[last_name]"]');
    var date_of_birth = $('input[name="user[date_of_birth]"]');
    var email = $('input[name="user[email]"]');
    var nric = $('input[name="user[nric]"]');
    var gender = $('input[name="user[gender]"]');
    var password = $('input[name="password"]');
    var password_confirmation = $('input[name="password_confirmation"]');

    var errors = [];

    if (first_name.val() == ''){
      errors.push('First name can\'t be blank');
    }

    if (last_name.val() == ''){
      errors.push('Last name can\'t be blank');
    }

    if (date_of_birth.val() == ''){
      errors.push('Date of birth can\'t be blank');
    } else {
      var age = getAge(date_of_birth.val());
      if (age < min_age || age > max_age){
        errors.push('Age must be between ' + min_age + ' to ' + max_age + ' years old');
      }
      console.log('age: ' + age);
    }

    if (email.val() == ''){
      errors.push('Email can\'t be blank');
    } else {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.val())){  
        var valid_email = validateEmailUniqueness(email.val());
        if (valid_email.length > 0){
          for (var i = 0; i < valid_email.length; i ++){
            errors.push (valid_email[i]);
          }
        }
      } else {
        errors.push('Invalid email format');
      }
    }

    if (nric.val() == ''){
      errors.push('NRIC/FIN can\'t be blank');
    }

    if (!$('input[name="user[gender]"]:checked').val()){
      errors.push('Please select a gender');
    }

    var empty_pass = false;
    if (password.val() == ''){
      empty_pass = true;
      errors.push('Password can\'t be blank');
    }
    if (password_confirmation.val() == ''){
      empty_pass = true;
      errors.push('Password confirmation can\'t be blank');
    }
    if (!empty_pass){
      if (password.val() != password_confirmation.val()){
        errors.push('Password confirmation don\'t match');
      } else {
        var min_pass_length = 6;
        if (password.val().length < min_pass_length){
          errors.push('Password lenght must be at least ' + min_pass_length + ' characters');
        }
      }
    }

    return errors;

  }

  function getAge(dob){
    var str = dob.split('/');
    var firstdate = new Date(str[2],str[1],str[0]);
    var today = new Date();        
    var dayDiff = Math.ceil(today.getTime() - firstdate.getTime()) / (1000 * 60 * 60 * 24 * 365);
    var age = parseInt(dayDiff);
    return age;
  }

  function validateEmailUniqueness(email){
    var res = [];
    $.ajax({
      type: 'POST',
      async: false,
      url: '/admin/users/validate-email-uniqueness',
      data: {
        email: email
      },
      beforeSend: function(xhr){
        // $('#loader').show();
      },
      success: function(result){
        console.log('success -> result', result);
        res = result.errors;
        // $('#loader').hide();
      },
      error: function(result){
      }
    });
    return res;
  }

});
