$(document).ready(function(){

  $('#filter-form').on('submit', function(e){
    e.preventDefault();
    $('#form-errors').html('').hide();
    $('#product-result').html('<div style="color: green; font-style: italic; margin: 20px 0 0 0;">Loading...</div>');

    var form = $(this);
    var insurer_name = $('select[name="insurer_name"]');
    var coverage_type = $('select[name="coverage_type"]');
    var trip_type = $('select[name="trip_type"]');

    var errors = validateForm();
    var html_errors = '';

    if (errors.length > 0){
      for (var i = 0; i < errors.length; i ++){
        html_errors += '<li>' + errors[i] + '</li>';
      }
      $('#form-errors').html(html_errors).show();
      $('#product-result').html('');
    } else {
      console.log('Valid form....')

      var form_data = {
        insurer_name: insurer_name.val(),
        coverage_type: coverage_type.val(),
        trip_type: trip_type.val(),
      }

      $.ajax({
        type: 'POST',
        async: false,
        url: form.attr('action'),
        data: form_data,
        success: function(result){
          var html_str = '';
          if (result.products.length > 0){
            console.log(result.products);
            for (var i = 0; i < result.products.length; i ++){
              console.log('Name: ' + result.products[i]['name']);


              html_str += '<div class="item">';
                html_str += '<div class="details">';
                  html_str += '<span class="title">Insurer Name</span>';
                  html_str += '<span class="content">' + result.products[i]['insurer_name'] + '</span>';
                html_str += '</div>';
                html_str += '<div class="details">';
                  html_str += '<span class="title">Product Name</span>';
                  html_str += '<span class="content">' + result.products[i]['name'] + '</span>';
                html_str += '</div>';
                html_str += '<div class="details">';
                  html_str += '<span class="title">Coverage Type</span>';
                  html_str += '<span class="content">' + result.products[i]['coverage_type'] + '</span>';
                html_str += '</div>';
                html_str += '<div class="details">';
                  html_str += '<span class="title">Trip Type</span>';
                  html_str += '<span class="content">' + result.products[i]['trip_type'] + '</span>';
                html_str += '</div>';
                html_str += '<div class="details">';
                  html_str += '<span class="title">Premium</span>';
                  html_str += '<span class="content">' + $.number(result.products[i]['premium'], 2); + '</span>';
                html_str += '</div>';
              html_str += '</div>';

            }
          }
          $('#product-result').html(html_str);
        },
        error: function(result){
          $('#product-result').html('');
        }
      });

    }
    return false;
  });


  function validateForm(){

    var insurer_name = $('select[name="insurer_name"]');
    var coverage_type = $('select[name="coverage_type"]');
    var trip_type = $('select[name="trip_type"]');

    var errors = [];

    if (insurer_name.val() == ''){
      errors.push('Insurer can\'t be blank');
    }

    if (coverage_type.val() == ''){
      errors.push('Coverage Type can\'t be blank');
    }

    if (trip_type.val() == ''){
      errors.push('Trip Type can\'t be blank');
    }

    return errors;

  }


});