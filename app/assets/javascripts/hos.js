$(document).ready(function(){

  $('#premium').number( true, 2 );

  $('#product-form').on('submit', function(e){
    e.preventDefault();
    $('#form-errors').html('').hide();

    var form = $(this);
    var authenticity_token = $('input[name="authenticity_token"]');
    var name = $('input[name="product[name]"]');
    var premium = $('input[name="product[premium]"]');
    var insurer_name = $('input[name="product[insurer_name]"]');

    var errors = validateForm();
    var html_errors = '';

    if (errors.length > 0){
      for (var i = 0; i < errors.length; i ++){
        html_errors += '<li>' + errors[i] + '</li>';
      }
      $('#form-errors').html(html_errors).show();
    } else {

      var form_data = {
        authenticity_token: authenticity_token.val(),
        product:{
          name: name.val(),
          premium: premium.val(),
          coverage_type: $('input[name="product[coverage_type]"]:checked').val(),
          trip_type: $('input[name="product[trip_type]"]:checked').val(),
          insurer_name: insurer_name.val(),
        }
      }

      $.ajax({
        type: 'POST',
        async: false,
        url: form.attr('action'),
        data: form_data,
        success: function(result){
          if (result.status == 1){
            window.location.href = form.attr('redirect-url');
          }
        },
        error: function(result){
        }
      });

    }

    return false;
  });

  function validateForm(){

    var name = $('input[name="product[name]"]');
    var premium = $('input[name="product[premium]"]');
    var insurer_name = $('input[name="product[insurer_name]"]');

    var errors = [];

    if (name.val() == ''){
      errors.push('Product name can\'t be blank');
    }

    if (premium.val() == ''){
      errors.push('Premium can\'t be blank');
    }
  
    if (!$('input[name="product[coverage_type]"]:checked').val()){
      errors.push('Please select a Coverage type');
    }
  
    if (!$('input[name="product[trip_type]"]:checked').val()){
      errors.push('Please select a Trip type');
    }

    if (insurer_name.val() == ''){
      errors.push('Insurer name can\'t be blank');
    }

    return errors;

  }

});