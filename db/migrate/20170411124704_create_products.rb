class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string  "name"
      t.string  "premium"
      t.string  "coverage_type"
      t.string  "trip_type"
      t.string  "insurer_name"
      t.timestamps null: false
    end
  end
end
