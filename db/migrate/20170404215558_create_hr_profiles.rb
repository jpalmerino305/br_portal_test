class CreateHrProfiles < ActiveRecord::Migration
  def change
    create_table :hr_profiles do |t|
      t.string  "first_name"
      t.string  "last_name"
      t.date    "date_of_birth"
      t.string  "email"
      t.string  "nric"
      t.string  "gender"
      t.string  "role"
      t.timestamps null: false
    end
  end
end
