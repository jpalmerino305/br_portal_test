Rails.application.routes.draw do
  devise_for :user_logins
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'admin#index'

  get 'admin'                                   => 'admin#index'

  get 'admin/users/:role'                       => 'admin/users#index'
  get 'admin/users/:role/new'                   => 'admin/users#new'
  post 'admin/users/:role/new'                  => 'admin/users#create'
  get 'admin/users/:role/edit/:id'              => 'admin/users#edit'

  post 'admin/users/validate-email-uniqueness'  => 'admin/users#validate_email_uniqueness'

  get 'hos' => 'hos#index'

  get 'hos/products'                            => 'hos/products#index'
  get 'hos/products/new'                        => 'hos/products#new'
  post 'hos/products/new'                       => 'hos/products#create'
  get 'hos/products/edit/:id'                   => 'hos/products#edit'


  get 'hr' => 'hr#index'
  post 'hr/filter' => 'hr#filter'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
